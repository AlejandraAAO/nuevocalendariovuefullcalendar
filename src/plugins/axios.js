import axios from 'axios'

const axiosInstance = axios.create({
  baseURL:'http://192.168.1.240:3200/api'
  //produccion: http://104.237.152.84:3200/api
  // baseURL:'http://192.168.1.241:3200/api'
})
 export default({ Vue }) => {
   Vue.prototype.$axios = axiosInstance
 }
 
 export {axiosInstance} 

/* export default ({ Vue }) => {
  Vue.prototype.$axios = axios
} */

